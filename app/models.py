from app import db


class UrlShortener(db.Model):
    __tablename__="urlshortener"
    id                  =db.Column      (db.Integer,primary_key=True)
    oriurl              =db.Column      (db.String(250),unique=True)
    conurl              =db.Column      (db.String(250),unique=True)


    def __init__(self,oriurl):
        self.oriurl=oriurl


    def __repr__(self):
        return "Url %s with id %d"%(self.oriurl,self.id)
