from flask import jsonify,render_template,request,redirect
from app import app,db
from app.models import *

@app.route("/",methods=["GET"])
def shortenthis():
    return render_template("shortenthis.html")


@app.route("/convert",methods=["POST"])
def convert():
    # try:
    print request.json
    # print request.POST.get("link")
    link=request.json["link"]
    print link
    urlshortener=UrlShortener(link)
    db.session.add(urlshortener)
    db.session.commit()
    convert(urlshortener)
    return jsonify({"shortened_url":urlshortener.conurl})
    # except:
        # return api_error()

@app.route("/<path:path>",methods=["GET"])
def retrieve(path="abc"):
    try:
        urlshortener=UrlShortener.query.filter(UrlShortener.conurl==path)
        if urlshortener.count()==0:
            return render_template("404.html")
        else:
            urlshortener=urlshortener[0]
            url=urlshortener.oriurl
            return redirect(url)
    except:
        return render_template("404.html")



def convert(urlshortener):
    mainstring="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    l=[]
    for x in mainstring:
        l.append(x)
    toconvert=urlshortener.oriurl
    num=urlshortener.id
    s=''
    while num!=0:
        s=s+l[num%62]
        num/=62
    s=s[::-1]
    urlshortener.conurl=s
    db.session.add(urlshortener)
    db.session.commit()