$(document).ready(function() {

	// Initializing the variables
	var tbList = [
			$("#tb1"),
			$("#tb2"),
			$("#tb3")
		];
	var count = 0;
		
	// Jquery Functions for the associated functionality 
	// all funtionalities will run only after the "Comp It" button click	

	$("#btn").click(function(event) {
		event.preventDefault();
		
		var jsonData = {
			"link": $('#inp').val(),
		};
		console.log(jsonData);

		if (!checkForValidURL()) {
			$("#err-div").html("This is not a valid URL. Please try again.")
			$("#err-div").fadeIn(200);
			$("#inp").click( function(event) {
				$("#err-div").fadeOut(100);
			});

			return;
		}

		$.ajax({
			url: '/convert',
			data: JSON.stringify(jsonData, null, '\t'),
			type: 'post',
			contentType: "application/json;charset=utf-8",
			dataType: "json",
			error: function() {
				$('#err-div').html('<p> An error has occured </p>');
				$('#err-div').fadeIn(200);
				$("#inp").click( function(event) {
					$("#err-div").fadeOut(100);
				});
				// alert("error");
			},
			success: function(data) {
				printResult($("#inp").val(),data["shortened_url"]);
				// $("#inp").click
			}
			
		});

		var printResult = function(origURL,shortURL) {
			
			if (count <= 2) {
				tbList[count].find("span.orig").html(origURL);
				tbList[count].find("span.val").html("http://"+shortURL);
				if (count == 0) {$("#result-div").fadeIn(100);}
				tbList[count].fadeIn(200);
				count++;
			}
			else if (count > 2 && count <= 5) {
				tbList[count-3].fadeOut(100)
				tbList[count-3].find("span.orig").html(origURL);
				tbList[count-3].find("span.val").html("http://"+shortURL);
				tbList[count-3].fadeIn(200);
				count++;
				if (count >= 6) {count = 3;}
			}
		}

		$("button.ui.teal.button").click( function(event) {
			// $("button.ui.teal.button").closest(".ui.very.basic.table").find("span.val").html("xoxo");
			// alert("xoxo");

			var $temp = $("<input>")
		    $("body").append($temp);
		    $temp.val($("button.ui.teal.button").closest(".ui.very.basic.table").find("span.val").text()).select();
		    document.execCommand("copy");
		    $temp.remove();
		    // alert("copied");
		});
	});

	var checkForValidURL = function() {
		var check =  new RegExp("^http(s)?:\/\/(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$");
		var URL = $("#inp").val();
		return check.test(URL);
	}
});